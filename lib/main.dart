// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

// #docregion MyApp
class MyApp extends StatelessWidget {
  // #docregion build
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Startup Name Generator',
      home: RandomWords(),
    );
  }
  // #enddocregion build
}
// #enddocregion MyApp

// #docregion RWS-var
class RandomWordsState extends State<RandomWords> {
  final _biggerFont = const TextStyle(fontSize: 18.0);
  bool pressed = false;
  String text = "";

  List<String> tickets = [];
  // #enddocregion RWS-var

  // #docregion _buildSuggestions
  Widget _buildSuggestions() {
    return ListView.builder(
        padding: const EdgeInsets.all(16.0),
        itemCount: tickets.length * 2,
        itemBuilder: /*1*/ (context, i) {
          if (i.isOdd) return Divider(); /*2*/
          final index = i ~/ 2; /*3*/
          return _buildRow(index);
        });
  }
  // #enddocregion _buildSuggestions

  // #docregion _buildRow
  Widget _buildRow(int index) {
    
    return ListTile(
      title: Text(
        tickets[index],
        style: _biggerFont,
      ),
    );
  }
  // #enddocregion _buildRow
  void _getEnteredText(String inputTxt) {
    if (inputTxt.trim().length > 0) {
      this.text = inputTxt.trim();
    }
  }

  void _add() {
    this.setState(() {
        tickets.add(this.text);
        text = "";
    });
    Navigator.of(context).pop();
  }

   // Create a text controller. Later, use it to retrieve the
  // current value of the TextField.
  final myController = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the widget is removed from the
    // widget tree.
    myController.dispose();
    super.dispose();
  }

  void _showcontent() {
    showDialog<Null>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return new AlertDialog(
        title: new Text('Ticket Number'),
        content: new SingleChildScrollView(
          child: new ListBody(
            children: <Widget>[
              new Text('This is a Dialog Box. Click OK to Close.'),
              TextField(
                controller: myController,
                onChanged: _getEnteredText,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: 'Enter a search term'
                ),
              )
            ],
          ),
        ),
        actions: <Widget>[
          new FlatButton(
            child: new Text('Ok'),
            onPressed: _add,
          ),
        ],
      );
    },
  );
}

  // #docregion RWS-build
  @override
  Widget build(BuildContext context) {
    Widget itemToBeRendered =  _buildSuggestions();

    return Scaffold(
      appBar: AppBar(
        title: Text('Travel Details'),
      ),
      body: itemToBeRendered,
      floatingActionButton: FloatingActionButton(
        onPressed: _showcontent,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      )
    );
  }
  // #enddocregion RWS-build
  // #docregion RWS-var
}
// #enddocregion RWS-var

class RandomWords extends StatefulWidget {
  @override
  RandomWordsState createState() => RandomWordsState();
}